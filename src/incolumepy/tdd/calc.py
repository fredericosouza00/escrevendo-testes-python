# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'
import math

def div(n1,n2):
    if (n1 <= 0) or (n2 <= 0):
        raise ValueError('Somente operações com números naturais!')
    else:
        return n1//n2
def mult(n1,n2):
    return n1*n2

def divisao(n1,n2):
    return n1/n2

def pot(n1,n2):
    return math.pow(n1,n2)

def sub(n1,n2):
    return n1-n2

def soma(n1,n2):
    return n1+n2
