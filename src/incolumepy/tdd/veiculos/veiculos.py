# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'

from abc import ABC
from datetime import datetime


class Veiculo(ABC):
    ano: datetime = ''
    modelo = None
    fabricante = None
    velocidade = 0
    categoria = ['terrestre','aéreo','aquático','espacial']
    tipo = ''

    def __init__(self):
        pass


class Aeronave(Veiculo):
    pass

class Barco(Veiculo):
    pass

class Bicicleta(Veiculo):
    pass

class Carro(Veiculo):
    pass

