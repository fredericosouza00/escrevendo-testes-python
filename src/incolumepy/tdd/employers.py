# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'
from datetime import datetime



class Employee:
    def __init__(self,nome,born,salario):
        self.nome = nome
        self.born = born
        self.salario = salario

    def aumento_salario(self,aumento_de_salario):

        if isinstance(aumento_de_salario,float):
            salario_ajustado = self.salario*aumento_de_salario+self.salario
            self.salario = salario_ajustado
        if isinstance(aumento_de_salario,int):
            salario_ajustado = self.salario*(aumento_de_salario/100)+self.salario
            self.salario = salario_ajustado

    def born(self,data_nascimento):
        meses = {1: 'janeiro', 2: 'fevereiro', 3: 'março', 4: 'abril', 5: 'maio', 6: 'junho', 7: 'julho', 8: 'agosto',
                 9: 'setembro', 10: 'outubro', 11: 'novembro', 12: 'dezembro'}
        formato_datetime = datetime.strptime(data_nascimento,'%d/%m/%Y')
        final = datetime.fromtimestamp(formato_datetime.timestamp()).strftime('%dd de '+meses.get(formato_datetime.month)+' de %Y')
        self.born = final

